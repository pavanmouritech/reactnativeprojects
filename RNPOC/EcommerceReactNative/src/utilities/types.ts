import { Route, RouteProp } from '@react-navigation/native';

export type Navigation = {
  navigate: (scene: string,route?:RouteProp<any>) => void;
  goBack: () => void;
  toggleDrawer: () => void;
  reset: (scene: string,route?:RouteProp<any>) => void;
  // merge: Boolean;
  // name: string;
};

export interface AuthInfo {
  authorize?: boolean;
}

export type ContextType = {
  authInfos: AuthInfo[];
  saveAuth: (authInfo: AuthInfo) => void;
  updateAuth: (authInfo: AuthInfo) => void;
};

export type AxiosProps = {
  type: any;
  url: any;
  params?: any;
  header?: any;
};
