import React from "react";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Profile from "../screens/profile";
import CustomSidebarMenu from "../screens/customSidebarScreen";
import MyTabs from "./bottomRoot";


const Drawer = createDrawerNavigator();

const RootDrawer = () =>{
    return(
        <Drawer.Navigator initialRouteName=""
          drawerContentOptions={{
          activeTintColor: '#e91e63',
          itemStyle: {marginVertical: 5},
        }}
        drawerContent={(props) => <CustomSidebarMenu {...props} />}>
          <Drawer.Screen name="Dashboard" component={MyTabs} />
          <Drawer.Screen name="Profile" component={Profile} />
        </Drawer.Navigator>
    );
}

export default RootDrawer;