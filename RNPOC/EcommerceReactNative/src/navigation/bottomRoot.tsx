import React from 'react';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import Icon from 'react-native-vector-icons/FontAwesome5';
import { Categories, Generic, Text } from '../theme/colors';
import Wallet from '../screens/wallet';
import Statistic from '../screens/statistic';
import Profile from '../screens/profile';
import Home from '../screens/Home';


const Tab = createMaterialBottomTabNavigator();

const MyTabs = () =>{
    return (
        <Tab.Navigator
            initialRouteName="Home"
            activeColor={Text.green}
            inactiveColor={Generic.blackT30}
            barStyle ={{backgroundColor:(Text.white)}}
        >
        <Tab.Screen
          name="Home"
          component={Home}
          options={{
            tabBarLabel: 'Home',
            tabBarIcon: ({ color }) => (
              <Icon name="home" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="wallet"
          component={Wallet}
          options={{
            tabBarLabel: 'Wallet',
            tabBarIcon: ({ color }) => (
              <Icon name="cart-plus" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
          name="Statistic"
          component={Statistic}
          options={{
            tabBarLabel: 'Updates',
            tabBarIcon: ({ color }) => (
              <Icon name="chart-line" color={color} size={26} />
            ),
          }}
        />
        <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'Profile',
          tabBarIcon: ({ color }) => (
            <Icon name="user-check" color={color} size={26} />
          ),
        }}
      />
      </Tab.Navigator>
    );
};

export default MyTabs;
