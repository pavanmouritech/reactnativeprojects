import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Login from '../screens/login';
import DetailScreen from '../screens/detailScreen/DetailScreen';
import CartScreen from '../screens/cartScreen';
import { NavigationContainer } from '@react-navigation/native';
import RootDrawer from './drawerRoot';


const RootNavigation = () => {
    const Stack = createStackNavigator();
    return (
        <NavigationContainer>           
            <Stack.Navigator initialRouteName="Login">
                <Stack.Screen
                    name="Login"
                    component={Login}
                    options={{headerShown: false}}
                />
                <Stack.Screen
                    name="RootDrawer"
                    component={RootDrawer}
                    options={{headerShown:false}}
                />
                <Stack.Screen
                    name="DetailScreen"
                    component={DetailScreen}
                    options={{headerShown:false}}
                />
                    <Stack.Screen
                    name="CartScreen"
                    component={CartScreen}
                    options={{headerShown:false}}
                />
            </Stack.Navigator>
        </NavigationContainer>

    );
};

export default RootNavigation;
