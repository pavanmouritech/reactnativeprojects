import {Platform, StyleSheet} from 'react-native';
import { tcolors, tflexD, tsize } from '../theme';
import { Generic } from '../theme/colors';


const styles = StyleSheet.create({
  statusbar: {backgroundColor: tcolors.white},
      container: {
          width: '100%',
          paddingBottom: tsize.n4,
          flexDirection: tflexD.row,
          alignContent: tflexD.space_between,
          justifyContent: tflexD.center,
          backgroundColor: tcolors.white,
          alignItems: tflexD.center,
          ...Platform.select({
      ios: {
          shadowColor: '#979797',
          shadowOffset: {width: tsize.n0, height: tsize.n3},
          shadowOpacity: tsize.n1,
          shadowRadius: tsize.n15,
            },
      android: {
          elevation: tsize.n15,
            },
          }),
        },
      title: {
          flex: tsize.n7,
          alignSelf: tflexD.center,
          position: tflexD.absolute,
          bottom: tsize.n9,
        },
      titleText:{
          fontSize:16,
          fontWeight:'bold',
          color:Generic.black,
      }, 
      left: {
          flex: tsize.n1_5,
          flexDirection: tflexD.row,
          alignSelf: tflexD.center,
          paddingLeft: tsize.n8,
          paddingVertical: tsize.n4,
          alignItems: tflexD.center,
        },
      right: {
          flex: tsize.n1_5,
          alignSelf: tflexD.center,
          justifyContent: tflexD.center,
          alignItems: tflexD.flex_end,
          paddingRight: tsize.n8,
        },
      searchBar:{
        flexDirection:'row',
        margin:5,
        width:'80%',
        height:40,
        borderColor:Generic.blackT25,
        borderWidth:1,
        borderRadius:10,
        justifyContent:'space-between',
        alignItems:'center',
      },
      textInput:{
        fontSize:16,
        fontWeight:'bold',
        left:10,
      },
      search:{
        right:10,
        color:Generic.blackT45,
      },
      });
export default styles;
