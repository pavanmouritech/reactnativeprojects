import React, {memo} from 'react';
import {Text, Pressable, View, Button, TouchableOpacity} from 'react-native';
import {tsize} from '../theme';
import {useSafeAreaInsets} from 'react-native-safe-area-context';
import {Navigation} from '../utilities/types';
import Icon  from 'react-native-vector-icons/Ionicons';
import { Generic } from '../theme/colors';
import styles from './style';
import { TextInput } from 'react-native-gesture-handler';


type Props = React.ComponentProps<typeof View> & {
  title?: string;
  search?: string;
  drawer?: boolean;
  help?: boolean;
  navigation: Navigation;
  back:boolean;
  forward:boolean;
};

const Header = ({title, drawer, help, navigation,back,forward,search}: Props) => {
  const insets = useSafeAreaInsets();

  const openDrawer = () => {
    navigation.toggleDrawer();
  };
  //  const navDrawer = (navName: string) => {
  //     openDrawer();
  //    navigation.navigate(navName);
  //   };

  const moveBack = () => {
    navigation.goBack();
  };

      return (
          <View style={styles.statusbar}>
            <View style={[styles.container, {paddingTop: insets.top + tsize.n6}]}>
              {drawer ? (
                <Pressable onPress={openDrawer} style={styles.left}>
                    <Icon name = "menu" size={25}color={Generic.black}/>
                </Pressable>
              ) : null}
              {search ? (
                <View style={styles.searchBar}>
                    <TextInput 
                            style={styles.textInput}
                            placeholder='Search for a product,cloth...'
                            placeholderTextColor={Generic.gray}
                    />
                    <TouchableOpacity>
                            <Icon name = "search" size={25}style={styles.search}/>
                    </TouchableOpacity>
                </View>
              ) : null}
              {back ? (
                <Pressable onPress={moveBack} style={styles.left}>
                    <Icon name = "md-chevron-back-sharp" size={25}color={Generic.black}/>
                </Pressable>
              ) : null}
              {forward ? (
                <Pressable style={styles.right}>
                    <Icon name = "ellipsis-horizontal" size={25}color={Generic.black}/>
                </Pressable>
              ) : null}

              <View style={styles.title}>
                    <Text style={styles.titleText}>{title}</Text>
              </View>
              {help ? (
                <Pressable style={styles.right}>
                    <Icon name = "md-chevron-forward" size={25}color={Generic.black}/>
                </Pressable>
            ) : null}
            </View>
          </View>
      );
    };

export default memo(Header);
