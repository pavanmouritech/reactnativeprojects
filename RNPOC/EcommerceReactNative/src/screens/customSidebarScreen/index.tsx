import React from 'react';
import {SafeAreaView,Image,Text,TouchableOpacity} from 'react-native';
import {DrawerContentScrollView,DrawerItemList,} from '@react-navigation/drawer';
import { useNavigation } from '@react-navigation/native';
import styles from './styles';

const CustomSidebarMenu = (props) => {

  const navigation = useNavigation();

  const logOut = () => {
    try {
     //   navigation.navigate('');
    } catch (e) {
        console.log(e);
    }
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Image
        source={require('../../assets/mouritech.jpg')}
        style={styles.sideMenuProfileIcon}
      />
      <Text style={styles.textInput}>
        Pavan Kumar Reddy
      </Text>
      <DrawerContentScrollView {...props}>
        <DrawerItemList {...props} />
      </DrawerContentScrollView>
        <TouchableOpacity onPress={logOut}>
            <Text style={styles.signOut}>Sign Out</Text>
        </TouchableOpacity>
    </SafeAreaView>
  );
};

export default CustomSidebarMenu;
