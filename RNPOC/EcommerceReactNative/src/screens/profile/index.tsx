import React from 'react';
import {View,Text} from 'react-native';
import styles from './style';

const Profile = () =>{
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Profile Screen</Text>
        </View>
    );
};

export default Profile;
