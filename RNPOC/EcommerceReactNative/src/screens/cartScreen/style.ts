import { StyleSheet } from "react-native";
import { tcolors, tsize } from "../../theme";
import { Categories, Text } from "../../theme/colors";

const styles = StyleSheet.create({
    header:{
        flex:1,
        backgroundColor:tcolors.white
    },
    CartContainer: {
        flex:1,
        backgroundColor:'white',
    },
    seperator: {
        height: tsize.n2,
        backgroundColor: Categories.other.seprator,
        marginHorizontal: tsize.n18,
        marginTop:(tsize.n1),
    },
    headerText:{
        flexDirection:'row',
        padding:10,
        margin:10,
        justifyContent:'space-evenly',
    },
    text1:{
        fontSize:15,
        fontWeight:'bold',
        color:'black',
        right:20,
    },
    text2:{
        fontSize:15,
        fontWeight:'bold',
        color:Text.green,
        left:20,
    },
    box:{
        width:150,
        height:150,
        backgroundColor:Categories.other.background,
        borderRadius:10,
        padding:10,
        margin:15,
        flexDirection:'row'
    },
    image:{
        width: 130, 
        height: 120, 
        borderRadius: 10,
    },
    titleView:{
        left:20,
        bottom:10,
        flexDirection:'column'
    },
    title:{
        margin:5,
        fontSize:18,
        color:'black',
        fontWeight:'bold',
        maxWidth:'82%',
    },
    description:{
        margin:5,
        fontSize:15,
        color:Text.gray,
    },
    priceView:{
        flexDirection:'row',
        justifyContent:'space-around',
        margin:-10,
        top:15,
    },
    price:{
        fontSize:15,
        fontWeight:'bold',
        margin:16,
        color:Text.black,
    },
    countBox:{
        height:30,
        width:50,
        backgroundColor:Text.green,
        borderRadius:8,
        margin:10,
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        maxWidth:25
      },
      countText:{
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
      },
      coupon:{
        margin: 15,
        height: 50,
        borderColor: 'gray',
        borderWidth: 2,
        borderRadius:10,
        marginTop:3,
        justifyContent:'center',
    },
    couponView:{
        flexDirection:'row', 
        alignItems:'center',
        justifyContent:'space-between',
    },
    couponText1:{
        fontSize:15,
        fontWeight:'bold',
        margin:10,
        color:'gray',
    },
    couponText2:{
      fontSize:15,
      fontWeight:'bold',
      color:Text.green,
      right:10,
    },
    couponView1:{
      flexDirection:'row', 
      alignItems:'center',
      margin:10,
    },
    border:{
      margin:20,
      height:50,
      borderRadius:10,
      backgroundColor:Text.green,
      borderWidth: 1,
      borderColor:Text.white,
      justifyContent:'center',
      alignItems:'center',
    },
    continue:{  
      fontSize:18,
      fontWeight:'bold',  
      color:'white',
    },
});
export default styles;