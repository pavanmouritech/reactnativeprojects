import React from "react";
import {Image, SafeAreaView, Text,TouchableOpacity,View} from 'react-native';
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { useDispatch, useSelector } from "react-redux";
import Header from "../../components/Header";
import { decrement, increment } from "../../redux/slice/counter";
import { Navigation } from "../../utilities/types";
import styles from "./style";


type Props = {
    navigation: Navigation;
}
export default function CartScreen({route,navigation}:Props) {
    
    const {item} = route.params;
        console.log(item);
    
        const dispatch = useDispatch();
        const count = useSelector(state => state.counter.value)
        console.log(count)

              return (
                <SafeAreaView style={styles.header}>
                    <Header
                        back={true}
                        title="MY CART"
                        forward={true}
                        navigation={navigation}
                    />
                    <View style={styles.CartContainer}>
                    <View style={styles.seperator}></View>
                        <View style={styles.headerText}>
                            <Icon name="account-circle" size={25}color={"black"}style={{right:20}}/>
                            <Text style={styles.text1}>Deliver to Jannah</Text>
                            <Text style={styles.text2}>San Diego, California</Text>
                            <Icon name="arrow-down-drop-circle" size={25}color={"#00e68a"}style={{left:20}}/>
                        </View>
                    <View style={styles.seperator}></View>    
                        <View style={styles.box}>
                                <View style={{flexDirection:'row'}}>
                                    <Image style={styles.image}
                                            source={{
                                                uri: item.image,
                                            }}
                                    />
                                </View>
                        <View style={styles.titleView}>
                                <View style={{flexDirection:'row'}}>
                                    <Text style={styles.title}numberOfLines={1}>{item.title}</Text>
                                </View>
                            <Text style={styles.description}numberOfLines={2}>{item.description}</Text>  
                        <View style={styles.priceView}>
                            <Text style={styles.price}>$ {item.price*count}</Text>
                                <TouchableOpacity style={styles.countBox} onPress={()=>dispatch(decrement())}>
                                    <Icon name="minus" size={25}color={"white"}/>
                                </TouchableOpacity>  
                                <View style={styles.countText}>  
                                    <Text style={{fontSize:18,fontWeight:'bold'}}>{count}</Text>
                                </View>  
                                <TouchableOpacity style={styles.countBox} onPress={()=>dispatch(increment())}>  
                                    <Icon name="plus" size={25}color={"white"}/>
                                </TouchableOpacity>
                        </View>
                    </View>
                </View>
                <View style={styles.seperator}></View> 
                <View style={{flexDirection:'row'}}>
                            <Text style={{margin:16,fontWeight:'bold'}}>Have a coupon code? enter here</Text>
                            <Icon name='hand-pointing-down' size={25}color={'yellow'}style={{top:15,right:12}}/>
                </View>
                    <TouchableOpacity style={styles.coupon}>
                        <View style={styles.couponView}> 
                                <Text style={styles.couponText1}>AXDSFR</Text>
                            <View style={styles.couponView1}>
                                <Text style={styles.couponText2}>Available</Text>
                                <Icon name='check-circle' size={25}color={'#00e68a'}/>
                            </View>
                        </View>
                    </TouchableOpacity>
                    <View style={{margin:5}}>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10}}>
                            <Text style={{fontSize:18,color:'gray',fontWeight:'bold'}}>Subtotal :</Text>
                            <Text style={{fontSize:18,color:'black',fontWeight:'bold'}}>$ {item.price*count}</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10}}>
                            <Text style={{fontSize:18,color:'gray',fontWeight:'bold'}}>Delivery Fee :</Text>
                            <Text style={{fontSize:18,color:'black',fontWeight:'bold'}}>$ 50</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10}}>
                            <Text style={{fontSize:18,color:'gray',fontWeight:'bold'}}>Discount :</Text>
                            <Text style={{fontSize:18,color:'black',fontWeight:'bold'}}>{item.price*count%30}</Text>
                        </View>
                    </View>
                    <View style={styles.seperator}></View>
                        <View style={{flexDirection:'row',justifyContent:'space-between',padding:10,margin:10}}>
                            <Text style={{fontSize:18,color:'black',fontWeight:'bold'}}>Total :</Text>
                            <Text style={{fontSize:18,color:'#00e68a',fontWeight:'bold'}}>$ {item.price*count+50%30}</Text>
                        </View>
                        <TouchableOpacity style={styles.border}>
                            <Text style={styles.continue}>Payment</Text>
                        </TouchableOpacity>
            </View>
        </SafeAreaView>    
        );
    }