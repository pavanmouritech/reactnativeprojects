import React from 'react';
import {View,Text} from 'react-native';
import styles from './style';

const Statistic = () =>{
    return (
        <View style={styles.container}>
            <Text style={styles.text}>Statistic Screen</Text>
        </View>
    );
};

export default Statistic;
