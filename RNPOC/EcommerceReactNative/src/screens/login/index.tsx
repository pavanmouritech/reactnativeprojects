import React, {useState} from 'react';
import {View, Text, Image, TextInput, TouchableOpacity,KeyboardAvoidingView} from 'react-native';
import styles from './style';
import {Generic} from '../../theme/colors';
import { useNavigation } from '@react-navigation/native';
import RootDrawer from '../../navigation/drawerRoot';


const Login = () => {
    const navigation = useNavigation();
    const [username, setUserName] = useState('');
    const [password, setPassword] = useState('');

    const handleSubmitPress = () => {
        const strongRegex = new RegExp("^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");

        if (!strongRegex.test(username)) {
            showMessage(MESSAGE.email)
            return false;
        } else if (password.length < 8) {
            showMessage(MESSAGE.password);
            return false;
        }
            navigation.navigate(RootDrawer);
    }

    return (
            <KeyboardAvoidingView style={styles.container}>
                    <Image source={require('../../assets/mouritech.jpg')} />
                    <View style={styles.inputView}>
                        <TextInput
                            style={styles.TextInput}
                            placeholder="UserName"
                            placeholderTextColor={Generic.gray}
                            onChangeText={username => setUserName(username)}
                        />
                    </View>
                    <View style={styles.inputView}>
                        <TextInput
                            style={styles.TextInput}
                            placeholder="Password."
                            placeholderTextColor={Generic.gray}
                            secureTextEntry={true}
                            onChangeText={password => setPassword(password)}
                        />
                    </View>
                    <TouchableOpacity>
                        <Text style={styles.forgot_button}>Forgot Password?</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress = {handleSubmitPress} style={styles.loginBtn}>
                        <Text style={styles.loginText}>LOGIN</Text>
                    </TouchableOpacity>
            </KeyboardAvoidingView>
    );
};

export default Login;