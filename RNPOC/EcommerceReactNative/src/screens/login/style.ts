import {StyleSheet} from 'react-native';
import { tsize,tpercent,tflexD, categoriesColors, tcolors, tfweight } from '../../theme';
import { Generic, Text } from '../../theme/colors';

const styles = StyleSheet.create({
    container: {
        flex: tsize.n3,
        backgroundColor:tcolors.white,
        alignItems:tflexD.center,
        justifyContent:tflexD.center,
    },
    inputView: {
        backgroundColor:categoriesColors.other.background,
        borderRadius: tsize.n30,
        width:tpercent.p70,
        height: tsize.n50,
        alignItems:tflexD.center,
        marginTop:tsize.n30,
    },
    TextInput: {
        height:tsize.n30,
        flex:tsize.n1,
        padding:tsize.n10,
        marginLeft:tsize.n20,
        fontWeight:tfweight.fbold,
    },
    forgot_button: {
        height:tsize.n50,
        fontWeight:tfweight.fbold,
        marginTop:tsize.n20,
        color:Text.gray,
    },
    loginBtn: {
        width:tpercent.p70,
        borderRadius:tsize.n25,
        height:tsize.n50,
        alignItems:tflexD.center,
        justifyContent:tflexD.center,
        marginTop:tsize.n20,
        backgroundColor:Generic.green17T80,
    },
    loginText:{
        fontWeight:tfweight.fbold,
        fontSize:tsize.n20,
        color:Generic.gray,
    }
});

export default styles;
