import { StyleSheet } from "react-native";
import { tcolors } from "../../theme";
import { Categories, Generic, Text } from "../../theme/colors";
import  androidFonts from '../../utilities/fonts';

const styles = StyleSheet.create({
    container: {
        flex:1,
        alignItems:'center',
    },
    errorText:{
        alignContent: 'center'
    },
    header:{
        flex:1,
        backgroundColor:tcolors.white
    },
    content:{
        flexDirection:'row',
        justifyContent:'space-between',
        margin:10
    },
    content1:{
        flexDirection:'row',
        justifyContent:'space-between',
        margin:10,
    },
    text1:{
        fontSize:20,
        fontWeight:'bold',
        color:Text.black,
    },
    text2:{
        fontSize:20,
        fontWeight:'bold',
        color:Text.gray,
    },
    ProductContainer: {
        flex:1,
        backgroundColor:'white',
    },
    box:{
        width:180,
        height:150,
        backgroundColor:Categories.other.background,
        borderRadius:10,
        margin:8,
        alignItems:'center',
        justifyContent:'center',
    },
    image:{
        width: 150, 
        height: 120, 
        borderRadius: 10,
    },
    innerText:{
       flexDirection:'row',
        justifyContent:'space-between',
        margin:10
    },
    categoryText:{
        fontSize:15,
        fontWeight:'bold',
        color:Text.black,
        TextStyle:androidFonts.arialRegular,
    },
    priceText:{
        fontSize:15,
        fontWeight:'bold',
        color:Text.black,
    },
    title:{
      maxWidth:200,
      maxHeight:50,
      paddingHorizontal:10,
      fontSize:13,
      fontWeight:'bold',
      color:Text.gray,
    }
});
export default styles;