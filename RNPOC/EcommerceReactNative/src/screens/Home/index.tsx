import React, { useEffect } from 'react';
import { ActivityIndicator, FlatList, Text, TouchableOpacity, View, Image, SafeAreaView } from "react-native";
import Header from "../../components/Header";
import { useAppDispatch, useAppSelector } from "../../redux/hooks";
import { callAlbum, selectAlbumInfo } from '../../redux/slice';
import { Navigation } from "../../utilities/types";
import styles from './styles';

type Props = {
    navigation: Navigation;
}
const HomeScreen = ({navigation}:Props) => {
   // const navigation = useNavigation();
    const dispatch = useAppDispatch();
    const { isLoading, album, albumResponseError} = useAppSelector(selectAlbumInfo);
    console.log(albumResponseError);

    useEffect(() => {
        dispatch(callAlbum());
    }, [dispatch]);
    return isLoading ?
        (
            <View style={styles.container}>
                <ActivityIndicator size="large" color="#00ff" />
            </View>

        )
        : albumResponseError ? (<View style={styles.container}>
            <Text style={styles.errorText}>{albumResponseError.error}</Text>
        </View>) :
            (
                <SafeAreaView style={styles.header}>
                    {/* <Header
                        drawer={true}
                        search={true}
                        help={true}
                        navigation={navigation}
                    /> */}
                <View style={styles.content}>
                    <Text style={styles.text1}>Hot Sales</Text>
                    <Text style={styles.text2}>See all</Text>
                </View>
                    <FlatList
                        numColumns={2}
                        data={album}
                        keyExtractor={item =>item.id}
                        renderItem={
                            ({ item }) => {
                                return (                
                            <View>
                                <TouchableOpacity style={styles.ProductContainer} 
                                        onPress={()=>navigation.navigate('DetailScreen',({ item:item }))}>
                                    <View style={styles.box}>
                                        <Image style={styles.image}
                                            source={{uri: item.image}}
                                        />
                                    </View>
                                    <View style={styles.innerText}>
                                        <Text style={styles.categoryText}>{item.category}</Text>
                                        <Text style={styles.priceText}>${item.price}</Text>
                                    </View>
                                    <View>
                                        <Text style={styles.title}numberOfLines={2}>{item.title}</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                     );
                }}
            />
        </SafeAreaView>
    );
}

export default HomeScreen;