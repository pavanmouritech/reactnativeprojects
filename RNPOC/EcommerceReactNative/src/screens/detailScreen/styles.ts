import { StyleSheet } from "react-native";
import { tcolors, tsize } from "../../theme";
import { Categories, Generic, Text} from "../../theme/colors";

const styles = StyleSheet.create({
    header:{
      flex:1,
      backgroundColor:tcolors.white
    },  
    DetailContainer: {
      width:'100%',
      height:280,
      backgroundColor:Generic.blackT0,
      alignItems:'center',
      justifyContent:'center',
    },
    image:{
      width:'60%', 
      height: 220, 
    },
    modelView:{
      flexDirection:'row',
      justifyContent:'space-between',
      marginTop:5,
      margin:20
    },
    box:{
      height:30,
      width:120,
      alignItems:'center',
      justifyContent:'center',
      borderRadius:8,
      backgroundColor:Text.green,
    }, 
    titleView:{
      flexDirection:'column',
      padding:20,
    },
    title:{
      fontSize:20,
      fontWeight:'bold',
      color:Generic.black,
      bottom:25,
    },
    description:{
      fontSize:15,
      fontWeight:'bold',
      bottom:15,
    },
    priceView:{
      flexDirection:'row',
      justifyContent:'space-between',
      bottom:10,
    },
    price:{
      left:18,
      fontSize:20,
      fontWeight:'bold',
      color:Generic.black,
    },
    circle:{
      flexDirection:'row',
      right:18,
    },
    seperator: {
      height: tsize.n2,
      backgroundColor: Categories.other.seprator,
      marginHorizontal: tsize.n18,
      marginTop:(tsize.n1),
    },
    coupon:{
      margin: 15,
      height: 50,
      borderColor: 'gray',
      borderWidth: 2,
      borderRadius:10,
      marginTop:5,
      justifyContent:'center',
    },
    couponView:{
      flexDirection:'row', 
      alignItems:'center',
      justifyContent:'space-between',
    },
    couponText1:{
      fontSize:15,
      fontWeight:'bold',
      margin:10,
      color:'gray',
    },
    couponText2:{
      fontSize:15,
      fontWeight:'bold',
      color:Text.green,
      right:10,
    },
    couponView1:{
      flexDirection:'row', 
      alignItems:'center',
      margin:10,
    },
    countView:{
      flexDirection:'row',
      marginTop:2,
    },
    countBox:{
      height:30,
      width:50,
      backgroundColor:Text.green,
      borderRadius:8,
      margin:20,
      flexDirection:'row',
      justifyContent:'center',
      alignItems:'center',
      maxWidth:28
    },
    countText:{
      flexDirection:'row',
      justifyContent:'center',
      alignItems:'center',
    },
    continueBox:{
      height:50,
      width:160,
      backgroundColor:Text.green,
      borderRadius:10,
      margin:10,
      left:50,
      justifyContent:'center',
      alignItems:'center',
    },
    continue:{
      fontSize:15,
      color:'white',
      fontWeight:'bold',
    }   
});
export default styles;