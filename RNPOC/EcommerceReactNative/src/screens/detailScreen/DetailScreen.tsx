import React from "react";
import {Image, SafeAreaView, Text,TouchableOpacity,View} from 'react-native';
import Header from "../../components/Header";
import { Navigation } from "../../utilities/types";
import styles from "./styles";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";
import { decrement, increment } from "../../redux/slice/counter";
import { useDispatch, useSelector } from "react-redux";


type Props = {
    navigation: Navigation;
}
export default function DetailScreen({route,navigation}:Props) {

    const count = useSelector(state => state.counter.value)
    const dispatch = useDispatch();
    console.log(count);

    const {item} = route.params;
    console.log(item);
    
            return (
                <SafeAreaView style={styles.header}>
                    <Header
                        back={true}
                        title="PRODUCT DETAILS"
                        forward={true}
                        navigation={navigation}
                    />
                    <View style={styles.DetailContainer}>
                        <Image style={styles.image}
                            source={{uri: item.image}}
                        />
                    </View>
                    <View style={styles.modelView}>
                        <View style={styles.box}>
                            <Text style={{color:"white",fontWeight:'bold'}}>Free Shipping</Text>
                        </View>
                        <View>
                            <Icon name = 'heart-circle-outline' size={30} color={'lightgray'}/>
                        </View>
                    </View>    
                    <View style={styles.titleView}>
                       <Text style={styles.title}numberOfLines={1}>{item.title}</Text>
                        <Text style={styles.description}numberOfLines={2}>{item.description}</Text>
                    </View>
                    <View style={styles.priceView}>
                        <Text style={styles.price}>$ {item.price*count}</Text>
                    <View style={styles.circle}>   
                        <Icon name='checkbox-blank-circle' size={25} color={'black'}/>
                        <Icon name='checkbox-blank-circle' size={25} color={'gray'}/>
                        <Icon name='checkbox-blank-circle' size={25} color={'lightgray'}/>
                    </View>
                    </View> 
                    <View style={styles.seperator}></View>
                        <View style={{flexDirection:'row'}}>
                            <Text style={{margin:16,fontWeight:'bold'}}>Have a coupon code? enter here</Text>
                            <Icon name='hand-pointing-down' size={25}color={'yellow'}style={{top:15,right:12}}/>
                        </View>
                        <TouchableOpacity style={styles.coupon}>
                            <View style={styles.couponView}> 
                                <Text style={styles.couponText1}>AXDSFR</Text>
                                <View style={styles.couponView1}>
                                    <Text style={styles.couponText2}>Available</Text>
                                    <Icon name='check-circle' size={25}color={'#00e68a'}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                    <View style={styles.seperator}></View>
                    <View style={styles.countView}>
                        <TouchableOpacity style={styles.countBox} 
                            onPress={()=>dispatch(decrement())}>
                            <Icon name="minus" size={25}color={"white"}/>
                        </TouchableOpacity>  
                        <View style={styles.countText}>  
                            <Text style={{fontSize:18,fontWeight:'bold'}}>{count}</Text>
                        </View>  
                        <TouchableOpacity style={styles.countBox} 
                            onPress={()=>dispatch(increment())}>  
                           <Icon name="plus" size={25}color={"white"}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.continueBox}
                              onPress={()=>navigation.navigate('CartScreen',({ item:item }))}>    
                            <Text style={styles.continue}>ADD TO CART</Text>
                        </TouchableOpacity>
                    </View>    
        </SafeAreaView>    
        );
    }
