export default {
  name: 'Name',
  email: 'Email',
  userName:'UserName',
  password:'PassWord',
  search:'Search for a product, cloth...',
  hotSales:'Hot Sales',
  seeAll:'See all',
  recentlyViewed:'Recently Viewed',
};
