const FLEXDIRECTIONS = {
  column: 'column' as 'column',
  row: 'row' as 'row',
  center: 'center' as 'center',
  flex_start: 'flex-start' as 'flex-start',
  flex_end: 'flex-end' as 'flex-end',
  space_between: 'space-between' as 'space-between',
  space_around: 'space-around' as 'space-around',
  space_evenly: 'space-evenly' as 'space-evenly',
  stretch: 'stretch' as 'stretch',
  baseline: 'baseline' as 'baseline',
  absolute: 'absolute' as 'absolute',
  slide_in_up : 'slideInUp' as 'slideInUp',
  solid : 'solid' as 'solid',
  cover:'cover' as 'cover',
  zoomInUp:'zoomInUp' as 'zoomInUp',
  hidden:'hidden'as 'hidden',
  left: 'left' as 'left',
  right: 'right' as 'right'
};
export default FLEXDIRECTIONS;
