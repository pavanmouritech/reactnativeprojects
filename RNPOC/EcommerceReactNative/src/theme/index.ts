import NUMERIC from './numeric';
import STRINGS from './strings';
import FLEXDIRECTIONS from './flexdirection';
import {FONTWEIGHT, PERCENT} from './weightpercent';
import ERRORSTRINGS from './errorstrings';
import { Categories, GRADIENTS, COLORS} from './colors';

export {
  NUMERIC as tsize,
  STRINGS as tstring,
  FLEXDIRECTIONS as tflexD,
  FONTWEIGHT as tfweight,
  PERCENT as tpercent,
  ERRORSTRINGS as tEstring,
  COLORS as tcolors,
  GRADIENTS as gradients,
  Categories as categoriesColors,
};
