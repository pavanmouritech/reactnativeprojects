const ERRORSTRINGS = {
  name: 'Name',
  email: 'Email',
  mobile: 'Mobile',
  password: 'Password',
  confirm_pass: 'Confirm Password',
  reset_pass: 'Reset Password',
  sign_in: 'SIGN IN',
  sign_up: 'SIGN UP',
  enter_full: 'Enter your fullname',
  enter_email: 'Enter your email address',
  enter_mobile: 'Enter your mobile number',
  valid_mobile: 'Enter your valid mobile number',
  enter_pwd: 'Enter a password',
  enter_con_pwd: 'Enter a confirm password',
  dont_have_acct: "Don't have an account?",
  valid_email: 'Please enter valid email address',
  valid_password:
    'Must contain Minimum 8 characters, at least 1 uppercase letter ,1 lowercase letter,1 number and 1 special character',
  valid_con_pwd: 'Password does not match',
  required_field: 'This field is required',
};
export default ERRORSTRINGS;
