import { combineReducers } from "redux";
import albumReducer from '../slice';
import counterReducer from '../slice/counter';

const rootReducer = combineReducers({

  album:albumReducer,
  counter: counterReducer,
 
});

export default rootReducer;
