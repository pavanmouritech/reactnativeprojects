import React from "react";
import {View} from 'react-native';
import { Provider } from "react-redux";
import { RootNavigator } from "./src/navigation/rootnavigaiton";
import { store } from "./src/screens/redux/store";


const App = () =>{
  
    return(
        <View style={{flex:1}}>
            <Provider store={store}>
                  <RootNavigator /> 
            </Provider>  
        </View>
  );
};

export default App;
